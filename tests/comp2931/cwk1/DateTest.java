package comp2931.cwk1;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by jamiestevens on 09/11/2017.
 */
public class DateTest {
    
    private Date testDate;
    
    @Before
    public void setUp() {
        testDate = new Date(2017, 12, 31);
    }
    
    @Test
    public void toStringTest() {
        
        assertThat(testDate.toString(), is("2017-12-31"));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void invalidDates() {
        new Date(-1, 0, 0);
        new Date(0, -1, 0);
        new Date(0, 0, -1);
        new Date(0, 13, 0);
        new Date(0, 2, 29);
        new Date(0, 0, 32);
        new Date(0, 9, 31);
        new Date(0, 4, 31);
        new Date(0, 6, 31);
        new Date(0, 11, 31);
    }
    @Test
    public void getDayOfYear() {
        assertThat(testDate.getDayOfYear(), is(365));
    }
    
}