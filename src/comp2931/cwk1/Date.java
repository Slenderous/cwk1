// Class for COMP2931 Coursework 1

package comp2931.cwk1;


/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    set(y, m, d);
    }
  
  
  private void set(int y, int m, int d) {
    if (y < 0) {
      throw new IllegalArgumentException("negative year impossible");
    }
    else if (m <= 0 || m > 12) {
      throw new IllegalArgumentException("months out of range");
    }
    else if (d <= 0 || d > 31) {
      throw new IllegalArgumentException("days out of range");
    }
    else {
      year = y;
      month = m;
      day = d;
    }
  }
  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }
  
  
  /**
   * Returns the day of year for this date.
   * Uses a for loop to add correct amount of days per calender month till 'month' is reached.
   *
   * @return Day Of Year
   */
  public int getDayOfYear(){
    int dayOfYear = 0;
    for (int i = 1; i <= month; i++) {
      if (i == 4 || i == 6 || i == 9 || i == 11){dayOfYear += 30;}
      else if (i == 2){dayOfYear += 28;}
      else{
        dayOfYear += 31;
      }
    }
    return dayOfYear+day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }
  
  /**
   * Replaces the standard object equals method.
   *
   * @return Day
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      // 'other' is same object as this one!
      return true;
    }
    else if (! (other instanceof Date)) {
      // 'other' is not a Date object
      return false;
    }
    else {
      // Compare fields
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
              && getMonth() == otherDate.getMonth()
              && getDay() == otherDate.getDay();
    }
  }

}
